#include <gtest/gtest.h>

#include <Uni/Logging/macros>

TEST(Logging, Printing) {
  logInfo("Test log");
  logInfo("Test log {1}",        1500111);
  logInfo("Test log {1}",        "Some");
  std::string shalom = "שלום";
  logInfo("Test log '{1}' of 4 symbols of length {2} chars",
      shalom,
      shalom.size());
  logInfo("Test log UTF-8 {1}",  u8"שלום");
  logInfo("Test log UTF-16 {1}", u"שלום");
  logInfo("Test log UTF-32 {1}", U"שלום");

  // String with L is coded in 16bit rather than 8bit, take an example:
  // "A" = 41 L"A" = 00 41
  logInfo("Test log wide chars {1}", L"שלום");
  std::cout << shalom << std::endl;
  std::cout << "UTF-8 " << u8"שלום" << std::endl;
  std::cout << "UTF-16 " << u"שלום" << std::endl;
  std::cout << "UTF-32 " << U"שלום" << std::endl;
  std::cout << "wide chars " << L"שלום" << std::endl;

  std::string systems = "системы";
  logInfo("Test log '{1}' of 7 symbols of length {2} chars",
      systems,
      systems.size());
  std::cout << systems << std::endl;

  std::string japanString = "動画、読書な";
  logInfo("Test log '{1}' of 6 symbols of length {2} chars",
      japanString,
      japanString.size());
  std::cout << japanString << std::endl;

}
