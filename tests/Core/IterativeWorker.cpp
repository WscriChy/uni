#include <gtest/gtest.h>

#include <Uni/IterativeWorker>

#include <vector>

namespace Uni {
namespace Test {
namespace Core {
class TestObject : public Uni::IterativeTask {
public:
  TestObject() {}

  virtual
  ~TestObject() {}

  void
  initialize() {}

  void
  iterate() {}

  void
  release() {}
};
}
}
}

TEST(IterativeWorker, ConstructionTest) {
  using TestObject = Uni::Test::Core::TestObject;

  Uni::IterativeWorker worker;
  auto            testTask = new TestObject();
  auto            task     = Uni::IterativeTask::Shared(testTask);
  worker.task(task);
  worker.start();
}

TEST(IterativeWorker, WaitConditionTesting) {
  using TestObject = Uni::Test::Core::TestObject;
  typedef std::unique_lock<std::mutex> UniqueLock;

  Uni::IterativeWorker worker;
  auto            testTask = new TestObject();
  auto            task     = Uni::IterativeTask::Shared(testTask);
  worker.task(task);
  worker.isWaitAtInitializationFinished(true);
  worker.isWaitAtIterationFinished(true);
  worker.isWaitAtReleaseFinished(true);

  bool                    isInitializationFinished = false;
  bool                    isIterationFinished      = false;
  bool                    isReleaseFinished        = false;
  std::mutex              mutex;
  std::condition_variable waitCondition;

  worker.observe(Uni::IterativeWorkerProperties::InitializationFinished,
                 [&] () {
                   std::unique_lock<std::mutex> internalLock(mutex);
                   isInitializationFinished = true;
                   internalLock.unlock();
                   waitCondition.notify_all();
                 });

  worker.observe(Uni::IterativeWorkerProperties::IterationFinished,
                 [&] () {
                   std::unique_lock<std::mutex> internalLock(mutex);
                   isIterationFinished = true;
                   internalLock.unlock();
                   waitCondition.notify_all();
                 });

  worker.observe(Uni::IterativeWorkerProperties::ReleaseFinished,
                 [&] () {
                   std::unique_lock<std::mutex> internalLock(mutex);
                   isReleaseFinished = true;
                   internalLock.unlock();
                   waitCondition.notify_all();
                 });

  worker.start();

  {
    UniqueLock lock(mutex);

    if ((worker.state() ==
         Uni::IterativeWorker::State::WaitingAtInitializationFinished) ||
        isInitializationFinished) {
      worker.endWaitAtInitializationFinished();
    } else {
      waitCondition.wait(lock);
      worker.endWaitAtInitializationFinished();
    }
  }

  {
    UniqueLock lock(mutex);

    if ((worker.state() ==
         Uni::IterativeWorker::State::WaitingAtIterationFinished) ||
        isIterationFinished) {
      worker.endWaitAtIterationFinished();
    } else {
      waitCondition.wait(lock);
      worker.endWaitAtIterationFinished();
    }
  }

  {
    UniqueLock lock(mutex);

    if ((worker.state() ==
         Uni::IterativeWorker::State::WaitingAtReleaseFinished) ||
        isReleaseFinished) {
      worker.endWaitAtReleaseFinished();
    } else {
      waitCondition.wait(lock);
      worker.endWaitAtReleaseFinished();
    }
  }
  worker.stop();
}
