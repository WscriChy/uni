#include <gtest/gtest.h>

#include <Uni/Observable>

#include <vector>

namespace Uni {
namespace Test {
namespace Core {
enum class TestObjectProperties {
  Int      = 0,
  IntFloat = 1,
};

class TestObject : public Observable<TestObjectProperties> {
public:
  TestObject(int intValue_,
             int floatValue_) : intValue(intValue_),
                                floatValue(floatValue_) {}

  void
  changeIntValue(int const& value) {
    intValue = value;
    notify(TestObjectProperties::Int,
           value);
  }

  void
  changeIntFloatValue(int const&   intValue_,
                      float const& floatValue_) {
    intValue   = intValue_;
    floatValue = floatValue_;

    notify(TestObjectProperties::IntFloat,
           intValue_,
           floatValue_);
  }

  int intValue;
  int floatValue;
};

class TestHandler {
public:
  void
  testIntObserverFunction(int const& value) {
    EXPECT_EQ(value, 20);
  }
  void
  testIntFloatObserverFunction(int const& intValue, float const& floatValue) {
    EXPECT_EQ(intValue,     120);
    EXPECT_EQ(floatValue, 11.3f);
  }
};

void
testIntObserverFunction(int const& value) {
  EXPECT_EQ(value, 20);
}
void
testIntFloatObserverFunction(int const& intValue, float const& floatValue) {
  EXPECT_EQ(intValue,     120);
  EXPECT_EQ(floatValue, 11.3f);
}
}
}
}

TEST(Observerable, LambdaTest) {
  using Uni::Test::Core::TestObject;
  using Uni::Test::Core::TestObjectProperties;
  using namespace Uni::Test::Core;

  TestObject object(10, 15.0f);

  object.observe(
    TestObjectProperties::Int,
    [] (int const& value) {
      EXPECT_EQ(value, 20);
    }
    );

  object.observe(
    TestObjectProperties::IntFloat,
    [] (int const& intValue, float const&  floatValue) {
      EXPECT_EQ(intValue,     120);
      EXPECT_EQ(floatValue, 11.3f);
    }
    );

  object.changeIntValue(20);
  object.changeIntFloatValue(120, 11.3f);
}

TEST(Observerable, FunctionPointerTest) {
  using Uni::Test::Core::TestObject;
  using Uni::Test::Core::TestObjectProperties;
  using namespace Uni::Test::Core;

  TestObject object(10, 15.0f);
  object.observe(
    TestObjectProperties::Int,
    &testIntObserverFunction
    );

  object.observe(
    TestObjectProperties::IntFloat,
    &testIntFloatObserverFunction
    );

  object.changeIntValue(20);
  object.changeIntFloatValue(120, 11.3f);
}

TEST(Observerable, BindObjectTest) {
  using Uni::Test::Core::TestObject;
  using Uni::Test::Core::TestObjectProperties;
  using namespace Uni::Test::Core;

  TestObject object(10, 15.0f);
  using std::placeholders::_1;
  using std::placeholders::_2;

  TestHandler handler;

  object.observe(TestObjectProperties::Int,
                 &handler, &TestHandler::testIntObserverFunction);
  object.observe(TestObjectProperties::IntFloat,
                 &handler, &TestHandler::testIntFloatObserverFunction);

  object.changeIntValue(20);
  object.changeIntFloatValue(120, 11.3f);
}

TEST(Observerable, FunctionObjectTest) {
  using Uni::Test::Core::TestObject;
  using Uni::Test::Core::TestObjectProperties;
  using namespace Uni::Test::Core;

  TestObject object(10, 15.0f);
  using std::placeholders::_1;
  using std::placeholders::_2;
  std::function<void(int const&)> function1 =
    std::bind(&testIntObserverFunction, _1);
  std::function<void(int const&, float const&)> function2 =
    std::bind(&testIntFloatObserverFunction, _1, _2);

  object.observe(TestObjectProperties::Int, function1);
  object.observe(TestObjectProperties::IntFloat, function2);

  object.changeIntValue(20);
  object.changeIntFloatValue(120, 11.3f);
}
