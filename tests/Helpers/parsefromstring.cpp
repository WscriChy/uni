#include <gtest/gtest.h>

#include <Uni/Helpers/parsefromstring>

#include <iostream>

using namespace Uni::Helpers;

TEST(parsefromstring, parse_integer_number) {
  std::string lines1[] = {
    "23",
    "   234238490",
    "              +423012    "
  };

  std::string lines2[] = {
    "-23420700",
    "-234889"
  };

  int results[] = {
    23,
    234238490,
    +423012,
    -23420700,
    -234889
  };

  int i = 0;

  for (auto const& line : lines1) {
    int      result_1;
    unsigned result_2;
    long     result_3;
    EXPECT_TRUE(parse_integer_number(line, result_1));
    EXPECT_EQ(results[i], result_1);
    EXPECT_TRUE(parse_integer_number(line, result_2));
    EXPECT_EQ(results[i], result_2);
    EXPECT_TRUE(parse_integer_number(line, result_3));
    EXPECT_EQ(results[i], result_3);
    ++i;
  }

  for (auto const& line : lines2) {
    int      result_1;
    unsigned result_2;
    long     result_3;
    EXPECT_TRUE(parse_integer_number(line, result_1));
    EXPECT_EQ(results[i], result_1);
    EXPECT_TRUE(parse_integer_number(line, result_2));
    EXPECT_EQ(results[i], result_2);
    EXPECT_TRUE(parse_integer_number(line, result_3));
    EXPECT_EQ(results[i], result_3);
    ++i;
  }
}

TEST(parsefromstring, parse_floating_point_number) {
  std::string lines[] = {
    "3.3",
    ".3",
    "  5E6",
    ".2e-14   ",
    "+.2e-14",
    "-.2e-14",
    "   7E+3    ",
    " 4.",
    "+4.    ",
    " -4.     ",
    "5.E2",
    "1e2",
    "3.14159265358979323846264338328",
    "20"
  };

  long double results[] = {
    3.3,
    .3,
    5E6,
    .2e-14,
    +.2e-14,
    -.2e-14,
    7E+3,
    4.,
    +4.,
    -4.,
    5.E2,
    1e2,
    3.14159265358979323846264338328,
    20
  };

  int i = 0;

  for (auto const& line : lines) {
    float       result_1 = results[i] - 10.0;
    double      result_2 = results[i] - 10.0;
    long double result_3 = results[i] - 10.0;
    EXPECT_TRUE(parse_floating_point_number(line, result_1));
    EXPECT_EQ((float)results[i], result_1);
    EXPECT_TRUE(parse_floating_point_number(line, result_2));
    EXPECT_EQ(results[i], result_2);
    EXPECT_TRUE(parse_floating_point_number(line, result_3));
    EXPECT_TRUE(std::fabs(results[i] - result_3)
                <= std::numeric_limits<double>::epsilon());
    ++i;
  }
}

TEST(parsefromstring, parse_integer_vector) {
  std::string lines[] = {
    "33,33,33, 080",
    "3 ,,   56  214  , 080",
    "+214 -214   73    , 080",
    " 4 +4 ,  -4     , 080",
    "    52 ;,    12 ;, , 314159265, 080"
  };

  Eigen::Matrix<int, 4, 1> results[] = {
    { 33,   33,    33,
      80                               },
    { 3,    56,    214,
      80                               },
    { +214, -214,  73,
      80                               },
    { 4,    +4,    -4,
      80                               },
    { 52,   12,    314159265,
      80                               }
  };

  int i = 0;

  for (auto const& line : lines) {
    Eigen::Matrix<int, 4, 1> result;
    EXPECT_EQ(parse_integer_vector(line, result), 4);
    EXPECT_EQ(results[i],                         result);
    ++i;
  }
}

TEST(parsefromstring, parse_floating_point_vector) {
  std::string lines[] = {
    "3.3,3.3,3.3",
    ".3 ,,   5E6  .2e-14  ",
    "+.2e-14 -.2e-14   7E+3    ",
    " 4. +4. ,  -4.     ",
    "    5.E2 ;, .  .. 1e2 ;, , 3.14159265358979323846264338328"
  };

  Eigen::Matrix<double, 3, 1> results[] = {
    { 3.3,     3.3,
      3.3                                                     },
    { .3,      5E6,
      .2e-14                                                  },
    { +.2e-14, -.2e-14,
      7E+3                                                    },
    { 4.,      +4.,
      -4.                                                     },
    { 5.E2,    1e2,
      3.14159265358979323846264338328                         }
  };

  int i = 0;

  for (auto const& line : lines) {
    Eigen::Matrix<double, 3, 1> result;
    EXPECT_EQ(parse_floating_point_vector(line, result), 3);
    // std::cout << result.transpose() << std::endl;
    EXPECT_EQ(results[i],                                result);
    ++i;
  }
}
