# General {{{
# ==============================================================================
# Require minimum version of CMake.
cmake_minimum_required(VERSION 2.8.12)
# ------------------------------------------------------------------------------
# Disable in-source builds.
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)
set(CMAKE_DISABLE_SOURCE_CHANGES  ON)

if ("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_BINARY_DIR}")
  message(SEND_ERROR "In-source builds are not allowed.")
endif ()
# ------------------------------------------------------------------------------
project("Uni" CXX C)
# ------------------------------------------------------------------------------
set(CMAKE_ERROR_DEPRECATED ON)
# ------------------------------------------------------------------------------
set(CMAKE_VERBOSE_MAKEFILE ON)
set(CMAKE_COLOR_MAKEFILE   ON)
# ------------------------------------------------------------------------------
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# ------------------------------------------------------------------------------
if (WIN32)
  set(CMAKE_SHARED_LIBRARY_PREFIX "")
endif ()
# ------------------------------------------------------------------------------
get_filename_component(PROJECT_DIR "." ABSOLUTE)
set(INSTALL_BINARY_DIR  bin)
set(INSTALL_INCLUDE_DIR include)
set(INSTALL_SOURCE_DIR source)
set(INSTALL_LIBRARY_DIR lib)
set(INSTALL_DOCUMENTATION_DIR documentation)
if (WIN32)
  set(INSTALL_CMAKE_DIR cmake)
else()
  set(INSTALL_CMAKE_DIR share/cmake/${PROJECT_NAME})
endif ()
# ==============================================================================
# }}} General

# Modules {{{
# ==============================================================================
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_SOURCE_DIR}/cmake/modules)
# ------------------------------------------------------------------------------
find_package(Threads REQUIRED)
find_package(Eigen REQUIRED)
find_package(Boost COMPONENTS filesystem locale regex REQUIRED)
set(Boost_USE_STATIC_LIBS    ON)
set(Boost_USE_MULTITHREADED  ON)
set(Boost_USE_STATIC_RUNTIME OFF)

set(Qt5_COMPONENTS Gui)
foreach (Qt5_COMPONENT ${Qt5_COMPONENTS})
  if (NOT Qt5${Qt5_COMPONENT}_FOUND)
    find_package(Qt5${Qt5_COMPONENT} QUIET)
    if (Qt5${Qt5_COMPONENT}_FOUND)
      get_target_property(Qt5_COMPONENT_LOCATION Qt5::${Qt5_COMPONENT} LOCATION)
      message(STATUS "Found Qt5${Qt5_COMPONENT}: ${Qt5_COMPONENT_LOCATION}")
    else()
      message(STATUS "Not Found Qt5${Qt5_COMPONENT}")
    endif ()
  endif ()
endforeach()
# ==============================================================================
# }}} Modules

# Compilers {{{
# ==============================================================================
if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  set(COMPILER_IS_CLANG TRUE)
  set(LINKER_IS_LD      TRUE)
elseif (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  set(COMPILER_IS_GCC TRUE)
  set(LINKER_IS_LD    TRUE)
elseif (CMAKE_CXX_COMPILER_ID STREQUAL "Intel")
  set(COMPILER_IS_ICC TRUE)
  set(LINKER_IS_ICC    TRUE)
else()
  message(FATAL "Unsopported toolchain")
endif ()
# ==============================================================================
# }}} Compilers

# Definitions {{{
# ==============================================================================
add_definitions(-DUNICODE)
set(UNICODE ON)
# ==============================================================================
# }}} Definitions

# Flags {{{
# ==============================================================================
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
if (COMPILER_IS_CLANG OR COMPILER_IS_GCC OR COMPILER_IS_ICC)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pipe")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
  #set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wextra")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pedantic")# -Wpedantic")
  #set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fmessage-length=0")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -fomit-frame-pointer")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -funroll-loops")
endif ()
# ------------------------------------------------------------------------------
if (LINKER_IS_LD)
  if (WIN32)
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static-libgcc")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static-libstdc++")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static")
  endif ()

  set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} -Wl,-O5")
  # ----------------------------------------------------------------------------
  if (WIN32)
    set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -static-libgcc")
    set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -static-libstdc++")
    set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -static")
  endif ()

  set(CMAKE_MODULE_LINKER_FLAGS_RELEASE "${CMAKE_MODULE_LINKER_FLAGS_RELEASE} -Wl,-O5")
  # ----------------------------------------------------------------------------
  if (WIN32)
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -static-libgcc")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -static-libstdc++")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -static")
  endif ()

  set(CMAKE_SHARED_LINKER_FLAGS_RELEASE "${CMAKE_SHARED_LINKER_FLAGS_RELEASE} -Wl,-O5")
elseif (LINKER_IS_ICC)
endif ()
# ==============================================================================
# }}} Flags

# 3rdParty {{{
# ==============================================================================
add_subdirectory("${PROJECT_DIR}/3rdParty/Pugi" ./3rdParty/Pugi)
add_subdirectory("${PROJECT_DIR}/3rdParty/GTest" ./3rdParty/GTest)
# ==============================================================================
# }}} 3rdParty

# Targets {{{
# ==============================================================================
file(GLOB_RECURSE CPP_FILES "${PROJECT_DIR}/source/*.cpp")
if (NOT Qt5Gui_FOUND)
  set(_FILE_LIST
    "source/InputMapping/Integration/Qt/qtkeyTo.cpp"
    )
  foreach(_FILE ${_FILE_LIST})
    get_filename_component(_FILE_ABS ${_FILE} ABSOLUTE)
    list(REMOVE_ITEM CPP_FILES ${_FILE_ABS})
  endforeach()
endif()

add_library(Uni STATIC ${CPP_FILES})

target_include_directories(Uni PRIVATE "${PROJECT_DIR}/source")

target_include_directories(Uni PUBLIC "${PROJECT_DIR}/include")

target_include_directories(Uni SYSTEM PUBLIC
  ${Eigen_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
  )

target_link_libraries(Uni PUBLIC
  ${Boost_LIBRARIES}
  )

if (Qt5Gui_FOUND)
  target_include_directories(Uni SYSTEM PUBLIC ${Qt5Gui_INCLUDE_DIRS})
  add_definitions(${Qt5Gui_DEFINITIONS})
  if (Qt5_POSITION_INDEPENDENT_CODE)
    set_target_properties(Uni PROPERTIES POSITION_INDEPENDENT_CODE
      ${Qt5_POSITION_INDEPENDENT_CODE})
  endif()
  target_link_libraries(Uni PUBLIC ${Qt5Gui_LIBRARIES})
endif()

install(DIRECTORY "${PROJECT_DIR}/include/"
  DESTINATION ${INSTALL_INCLUDE_DIR})

install(DIRECTORY "${PROJECT_DIR}/source/"
  DESTINATION ${INSTALL_SOURCE_DIR}
  PATTERN *.cpp EXCLUDE)

install(TARGETS Uni
        ARCHIVE DESTINATION ${INSTALL_LIBRARY_DIR}
        LIBRARY DESTINATION ${INSTALL_LIBRARY_DIR}
        RUNTIME DESTINATION ${INSTALL_BINARY_DIR})

install(FILES "${PROJECT_DIR}/cmake/${PROJECT_NAME}Config.cmake"
  DESTINATION ${INSTALL_CMAKE_DIR})
# ==============================================================================
# }}} Targets

# Tests {{{
# ==============================================================================
file(GLOB_RECURSE TESTS_CPP_FILES "${PROJECT_DIR}/tests/*.cpp")

add_executable(UniTests ${TESTS_CPP_FILES})

target_include_directories(UniTests SYSTEM PRIVATE
  "${PROJECT_DIR}/3rdParty/GTest/include")

target_link_libraries(UniTests PRIVATE
  ${CMAKE_THREAD_LIBS_INIT}
  Uni
  gtest
  gtest_main
  )

if (Testing)
  message(STATUS "Testing is enabled")
  enable_testing()
endif()

add_test(UniTests1 UniTests)
# ==============================================================================
# }}} Tests

# Documentation {{{
# ==============================================================================
# add a target to generate API documentation with Doxygen
#find_package(Doxygen)
#if(DOXYGEN_FOUND)
#  #  configure_file(${CMAKE_CURRENT_SOURCE_DIR}/.doxygen
#  #    ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)
#  file(TO_CMAKE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/documentation"
#    _DOXYGEN_OUTPUT_DIR)
#  file(TO_CMAKE_PATH "${_DOXYGEN_OUTPUT_DIR}/.doxygen"
#    _DOXYGEN_CONIFURATION_FILE)
#  add_custom_target(_documentation ALL
#    COMMAND "${DOXYGEN_EXECUTABLE}" "${_DOXYGEN_CONIFURATION_FILE}"
#    # The following should be ${doxyfile} only but it
#    # will break the dependency.
#    # The optimal solution would be creating a 
#    # custom_command for ${doxyfile} generation
#    # but I still have to figure out how...
#    #MAIN_DEPENDENCY ${_DOXYGEN_CONIFURATION_PATH}
#    DEPENDS Uni
#		WORKING_DIRECTORY "${_DOXYGEN_OUTPUT_DIR}"
#    COMMENT "Generating Doxygen documentation")
#   
#  install(DIRECTORY "${_DOXYGEN_OUTPUT_DIR}"
#    DESTINATION ${INSTALL_DOCUMENTATION_DIR})
#endif(DOXYGEN_FOUND)
# ==============================================================================
# }}} Documentation

# vim:ft=cmake:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
