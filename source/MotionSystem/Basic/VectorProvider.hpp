#ifndef Uni_MotionSystem_Basic_VectorProvider_hpp
#define Uni_MotionSystem_Basic_VectorProvider_hpp

#include "ScalarProvider.hpp"

#include <Uni/MotionSystem/Provider>

#include <Eigen/Core>

namespace Uni {
namespace MotionSystem {
namespace Basic {
/**
 * \brief
 * Template interface which represents a storage (state) of a vector value of
 * size N that could be updated and provide this information as well as the
 * vector value itself on request.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
template <class T,
          int N>
class VectorProvider : public Provider {
public:
  typedef Eigen::Matrix<T, N, 1> Vector;

public:
  VectorProvider() : Provider() {}

  VectorProvider(VectorProvider const& other) = delete;

  ~VectorProvider() {}

  VectorProvider const&
  operator=(VectorProvider const& other) = delete;

  virtual Vector
  values() const = 0;

  virtual T
  value(int const& index) const = 0;

  virtual void
  reset() = 0;

  T const&
  operator()(int const& index) const {
    return values(index);
  }
};
}
}
}

#endif
