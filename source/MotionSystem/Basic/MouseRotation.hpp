#ifndef Uni_MotionSystem_Basic_MouseRotation_hpp
#define Uni_MotionSystem_Basic_MouseRotation_hpp

#include "VectorProvider.hpp"

#include <Uni/MotionSystem/Direction>
#include <Uni/MotionSystem/ScreenState>

#include <Uni/Logging/macros>
#include <Uni/math>

#include <Eigen/Core>

#include <memory>

namespace Uni {
namespace MotionSystem {
namespace Basic {
/**
 * \brief
 * Template class which represents an entity of a mouse rotation of the
 * camera based on a 2D vector value that could be updated and provide this
 * information as well as the value itself on request.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
template <class T,
          class U               = T,
          Handedness handedness = Handedness::left>
class MouseRotation : public VectorProvider<T, 2> {
private:
  typedef VectorProvider<T, 2>           Base;
  typedef Math::Direction<T, handedness> Direction;

public:
  typedef typename Base::Vector                Vector;
  typedef ScreenState<U>                       ConcreteScreenState;
  typedef typename ConcreteScreenState::Shared ScreenStateShared;

public:
  MouseRotation() : Base(),
                    _isUpdated(false) {
    _vector.fill(0.0);
  }

  MouseRotation(MouseRotation const& other) = delete;

  MouseRotation const&
  operator==(MouseRotation const& other) = delete;

  void
  screenState(ScreenStateShared const& screenState) {
    _screenState = screenState;
  }

  bool
  isUpdated() const {
    return _isUpdated;
  }

  Vector
  values() const {
    return _vector;
  }

  T
  value(int const& index) const {
    return _vector(index);
  }

  void
  reset() {
    _vector.fill(0.0);
    _isUpdated = false;
  }

  void
  advance(T const& deltaX, T const& deltaY) {
    auto aspectRatio = T(_screenState->width()) / T(_screenState->height());
    auto tanY        = std::tan(T(_screenState->fovY()) / T(2));
    auto tanX        = tanY * aspectRatio;

    auto halfNearHeight = tanY;
    auto halfNearWidth  = tanX;

    auto nearYMove = halfNearHeight * (T(1) - 2 * std::abs(deltaY)
                                       / T(_screenState->height()));
    auto nearXMove = halfNearWidth *  (T(1) - 2 * std::abs(deltaX)
                                       / T(_screenState->width()));

    using Vector3 = typename Direction::Vector3;

    Vector3 from = Direction::forward();
    Vector3 toY  = from + nearYMove * Direction::up();

    Vector3 toX = from + nearXMove * Direction::right();

    _vector(0) +=  T(-sign(deltaY)) * std::acos(toY.normalized().dot(
                                                  from));
    _vector(1) +=  T(-sign(deltaX)) * std::acos(toX.normalized().dot(
                                                  from));
    _isUpdated = true;
  }

private:
  bool              _isUpdated;
  Vector            _vector;
  ScreenStateShared _screenState;
};

using MouseRotationd = MouseRotation<double>;
using MouseRotationf = MouseRotation<float>;
}
}
}

#endif
