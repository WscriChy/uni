#ifndef Uni_MotionSystem_Basic_ContinuousKeyPress_hpp
#define Uni_MotionSystem_Basic_ContinuousKeyPress_hpp

#include "ScalarProvider.hpp"

#include <Uni/Logging/macros>

#include <Uni/Stopwatch>

namespace Uni {
namespace MotionSystem {
namespace Basic {
/**
 * \brief
 * Template class which represents an entity of a continuous key press. It
 *  measures the amount of time a key was pressed.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
template <typename TDuration = Nanoseconds,
          typename TClock    = HighResolutionClock>
class ContinuousKeyPress
  : public ScalarProvider<typename Stopwatch<TDuration, TClock>::Duration> {
private:
  typedef Stopwatch<TDuration, TClock> ConcreteStopwatch;

public:
  typedef typename ConcreteStopwatch::Duration Duration;

private:
  typedef ScalarProvider<Duration> Base;

public:
  ContinuousKeyPress() : Base() {}

  ContinuousKeyPress(ContinuousKeyPress const& other) = delete;

  virtual
  ~ContinuousKeyPress() {}

  ContinuousKeyPress const&
  operator=(ContinuousKeyPress const& other) = delete;

  void
  start() {
    _stopwatch.start();
  }

  void
  stop() {
    _stopwatch.stop();
  }

  bool
  isUpdated() const {
    bool result =  _stopwatch.hasExpired(0);

    return result;
  }

  Duration
  reset() {
    Duration result = _stopwatch.elapsed();
    _stopwatch.reset();

    return result;
  }

  Duration
  value() const {
    return _stopwatch.elapsed();
  }

private:
  ConcreteStopwatch _stopwatch;
};
}
}
}

#endif
