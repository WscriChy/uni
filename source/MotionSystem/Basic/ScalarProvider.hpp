#ifndef Uni_MotionSystem_Basic_ScalarProvider_hpp
#define Uni_MotionSystem_Basic_ScalarProvider_hpp

#include <Uni/MotionSystem/Provider>

namespace Uni {
namespace MotionSystem {
namespace Basic {
/**
 * \brief
 * Template interface which represents a storage (state) of a scalar value that
 * could be updated and provide this information as well as the scalar value
 * itself on request.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
template <typename T>
class ScalarProvider : public Provider {
public:
  virtual
  ~ScalarProvider() {}

  virtual T
  value() const = 0;

  virtual T
  reset() = 0;
};
typedef ScalarProvider<double> ScalarProviderd;
}
}
}

#endif
