#ifndef Uni_MotionSystem_Basic_DummyMotion_hpp
#define Uni_MotionSystem_Basic_DummyMotion_hpp

#include <Uni/MotionSystem/Motion>

namespace Uni {
namespace MotionSystem {
namespace Basic {
/**
 * \brief
 * Template class which represents an entity of a dummy 3D motion.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
template <typename T>
class DummyMotion : public Motion<T> {
public:
  typedef Motion<T>             Base;
  typedef typename Base::Vector Vector;

public:
  DummyMotion() : Base() {}

  virtual
  ~DummyMotion() {}

  bool
  isUpdated() const { return false; }

  Vector
  xyz() const { return Vector(0.0); }

  T
  x() const { return 0.0; }

  T
  y() const { return 0.0; }

  T
  z() const { return 0.0; }

  void
  reset() {}
};
}
}
}

#endif
