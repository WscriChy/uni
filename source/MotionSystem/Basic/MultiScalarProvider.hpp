#ifndef Uni_MotionSystem_Basic_MultiScalarProvider_hpp
#define Uni_MotionSystem_Basic_MultiScalarProvider_hpp

#include "ScalarProvider.hpp"
#include "VectorProvider.hpp"

#include <Eigen/Core>

#include <array>
#include <memory>

namespace Uni {
namespace MotionSystem {
namespace Basic {
/**
 * \brief
 * Template class which represents a storage (state) of several (N) scalar
 * values.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
template <typename T, int N>
class MultiScalarProvider : public VectorProvider<T, N> {
private:
  typedef VectorProvider<T, N> Base;

public:
  typedef typename Base::Vector                   Vector;
  typedef ScalarProvider<T>                       ConcreteScalarProvider;
  typedef std::shared_ptr<ConcreteScalarProvider> ScalarProviderShared;
  typedef std::array<ScalarProviderShared, N>     ScalarProviders;

public:
  MultiScalarProvider() : Base() {
    _scalarProviders.fill(0);
  }

  MultiScalarProvider(
    MultiScalarProvider const& other) = delete;

  ~MultiScalarProvider() {}

  MultiScalarProvider const&
  operator=(MultiScalarProvider const& other) = delete;

  bool
  isUpdated() const {
    for (auto scalarProvider : _scalarProviders) {
      if (scalarProvider->isUpdated()) {
        return true;
      }
    }

    return false;
  }

  Vector
  values() const {
    Vector values;

    for (int i = 0; i < N; ++i) {
      values(i) = value(i);
    }

    return values;
  }

  T
  value(int const& index) const {
    return _scalarProviders[index]->value();
  }

  T
  operator()(int const& index) const {
    return value(index);
  }

  void
  reset() {
    for (auto& scalarProvider : _scalarProviders) {
      scalarProvider->reset();
    }
  }

  void
  scalarProviders(ScalarProviders const& scalarProviders) {
    _scalarProviders = scalarProviders;
  }

  void
  scalarProvider(int const&           index,
                 ScalarProviderShared scalarProvider) {
    _scalarProviders[index] = scalarProvider;
  }

private:
  ScalarProviders _scalarProviders;
};
}
}
}

#endif
