#ifndef Uni_MotionSystem_Motion_hpp
#define Uni_MotionSystem_Motion_hpp

#include "Provider.hpp"

#include <Eigen/Core>

namespace Uni {
namespace MotionSystem {
/**
 * \brief
 * Template interface which represents a storage (state) of a 3d motion.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
template <class T>
class Motion : public Provider {
public:
  typedef Eigen::Matrix<T, 3, 1> Vector;

public:
  Motion() : Provider() {}

  virtual
  ~Motion() {}

  virtual Vector
  xyz() const = 0;

  virtual T
  x() const = 0;

  virtual T
  y() const = 0;

  virtual T
  z() const = 0;

  virtual void
  reset() = 0;
};
}
}

#endif
