#ifndef Uni_MotionSystem_Camera_hpp
#define Uni_MotionSystem_Camera_hpp

#include "Direction.hpp"
#include "Handedness.hpp"

#include <Eigen/Geometry>

#include <algorithm>
#include <cassert>
#include <cmath>

namespace Uni {
namespace MotionSystem {
/**
 * \brief
 * Template class which represents an entity of the camera in 3D space.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
template <class T,
          Handedness handedness = Handedness::left>
class Camera {
  using AngleAxis  = Eigen::AngleAxis<T>;
  using Direction  = Math::Direction<T, handedness>;
  using Matrix4x4  = Eigen::Matrix<T, 4, 4>;
  using Quaternion = Eigen::Quaternion<T>;
  using Vector3    = Eigen::Matrix<T, 3, 1>;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  inline
  Camera(Vector3 const&    position    = Vector3::Zero(),
         Quaternion const& orientation = Quaternion::Identity())
    : _position(position),
      _orientation(orientation) {
    std::fill(std::begin(_old), std::end(_old), true);
  }

  /**
   * Return camera's position
   */
  inline Vector3 const&
  position() const
  { return _position; }

  /**
   * Return camera's orientation as a quaternion
   */
  inline Quaternion const&
  orientation() const
  { return _orientation; }

  inline Vector3 const&
  left() {
    if (_old[_left]) {
      _directions[_left] = _orientation * Direction::left();
      _old[_left]        = false;
    }

    return _directions[_left];
  }

  inline Vector3 const&
  right() {
    if (_old[_right]) {
      _directions[_right] = _orientation * Direction::right();
      _old[_right]        = false;
    }

    return _directions[_right];
  }

  inline Vector3 const&
  up() {
    if (_old[_up]) {
      _directions[_up] = _orientation * Direction::up();
      _old[_up]        = false;
    }

    return _directions[_up];
  }

  inline Vector3 const&
  down() {
    if (_old[_down]) {
      _directions[_down] = _orientation * Direction::down();
      _old[_down]        = false;
    }

    return _directions[_down];
  }

  inline Vector3 const&
  forward() {
    if (_old[_forward]) {
      _directions[_forward] = _orientation * Direction::forward();
      _old[_forward]        = false;
    }

    return _directions[_forward];
  }

  inline Vector3 const&
  backward() {
    if (_old[_backward]) {
      _directions[_backward] = _orientation * Direction::backward();
      _old[_backward]        = false;
    }

    return _directions[_backward];
  }

  inline void
  rotate(Quaternion const& rotation) {
    _orientation = _orientation * rotation;

    std::fill(std::begin(_old), std::end(_old), true);
  }

  inline void
  rotate(AngleAxis const& rotation)
  { rotate(Quaternion(rotation)); }

  inline void
  pitch(T radians)
  { rotate(AngleAxis(radians, Vector3::UnitX())); }

  inline void
  yaw(T radians)
  { rotate(AngleAxis(radians, Vector3::UnitY())); }

  inline void
  roll(T radians)
  { rotate(AngleAxis(radians, Vector3::UnitZ())); }

  inline void
  translate(Vector3 const& translation)
  { _position += translation; }

  inline void
  move(Vector3 const& movement)
  { translate(_orientation * movement); }

  /**
   * Produce view matrix in columns
   */
  inline Matrix4x4
  view1() {
    Matrix4x4 v = Matrix4x4::Identity();

    if (handedness == Handedness::right) {
      v(0, 0) = -left().x();
      v(0, 1) = -left().y();
      v(0, 2) = -left().z();

      v(0, 3) = position().x();
    } else {
      v(0, 0) = right().x();
      v(0, 1) = right().y();
      v(0, 2) = right().z();

      v(0, 3) = position().x();
    }

    v(1, 0) = up().x();
    v(1, 1) = up().y();
    v(1, 2) = up().z();

    v(1, 3) = position().y();

    v(2, 0) = -forward().x();
    v(2, 1) = -forward().y();
    v(2, 2) = -forward().z();

    v(2, 3) = position().z();

    return v.inverse();
  }

  /**
   * Produce view matrix in rows
   */
  inline Matrix4x4
  view2() {
    Matrix4x4 v = Matrix4x4::Identity();

    if (handedness == Handedness::right) {
      v(0, 0) = left().x();
      v(1, 0) = left().y();
      v(2, 0) = left().z();

      v(3, 0) = -position().dot(left());
    } else {
      v(0, 0) = right().x();
      v(1, 0) = right().y();
      v(2, 0) = right().z();

      v(3, 0) = -position().dot(right());
    }

    v(0, 1) = up().x();
    v(1, 1) = up().y();
    v(2, 1) = up().z();

    v(3, 1) = -position().dot(up());

    v(0, 2) = forward().x();
    v(1, 2) = forward().y();
    v(2, 2) = forward().z();

    v(3, 2) = -position().dot(forward());

    return v;
  }

  inline Matrix4x4
  view() {
    Matrix4x4 v = Matrix4x4::Identity();

    v(0, 0) = right().x();
    v(0, 1) = right().y();
    v(0, 2) = right().z();

    v(0, 3) = -right().dot(position());

    v(1, 0) = up().x();
    v(1, 1) = up().y();
    v(1, 2) = up().z();

    v(1, 3) = -up().dot(position());

    v(2, 0) = backward().x();
    v(2, 1) = backward().y();
    v(2, 2) = backward().z();

    v(2, 3) = -backward().dot(position());

    return v;
  }

  Matrix4x4
  projection(T width,
             T height,
             T fovY,
             T nearZ,
             T farZ) const {
    auto      aspectRatio = width / height;
    auto      f           = std::tan(fovY / T(2));
    Matrix4x4 p           = Matrix4x4::Zero();

    p(0, 0) = T(1) / (aspectRatio * f);
    p(1, 1) = T(1) / f;
    p(2, 2) = -(farZ + nearZ) / (farZ - nearZ);
    p(3, 2) = -T(1);
    p(2, 3) = -(T(2) * farZ * nearZ) / (farZ - nearZ);

    return p;
  }

private:
  Vector3    _position;
  Quaternion _orientation;
  Vector3    _directions[6];
  bool       _old[6];

  static int const _left  = 0;
  static int const _right = 1;

  static int const _up   = 2;
  static int const _down = 3;

  static int const _forward  = 4;
  static int const _backward = 5;
};

using Camerad = Camera<double>;
using Cameraf = Camera<float>;
}
}

#endif
