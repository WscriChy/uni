#ifndef Uni_MotionSystem_Provider_hpp
#define Uni_MotionSystem_Provider_hpp

namespace Uni {
namespace MotionSystem {
/**
 * \brief
 * Interface which represents a storage (state) of a value provider that could
 *  be updated and provide this information on request.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
class Provider {
public:
  virtual
  ~Provider() {}

  virtual bool
  isUpdated() const = 0;
};
}
}

#endif
