#ifndef Uni_MotionSystem_ScreenState_hpp
#define Uni_MotionSystem_ScreenState_hpp

#include <memory>

#include <Uni/Observable>

namespace Uni {
namespace MotionSystem {
enum ScreenStateProperty {
  Width  = 1,
  Height = 2,
  FovY   = 3,
  NearZ  = 4,
  FarZ   = 5
};

/**
 * \brief
 * Template class which represents a storage (state) of screen frustum in 3D
 *  space.
 *
 * \details
 *
 * \todo
 * 1. Rename to ScreenFrustum
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
template <typename T = float>
class ScreenState : public Observable<ScreenStateProperty> {
private:
  typedef Observable<ScreenStateProperty> Base;

public:
  typedef std::shared_ptr<ScreenState> Shared;

public:
  ScreenState() : Base(), _width(0), _height(0) {}

  ScreenState(ScreenState const& other) = delete;

  ScreenState const&
  operator==(ScreenState const& other) = delete;

  T const&
  width() const {
    return _width;
  }

  void
  width(T const& width) {
    _width = width;
    notify(ScreenStateProperty::Width, (T const&)_width);
  }

  T const&
  height() const {
    return _height;
  }

  void
  height(T const& height) {
    _height = height;
    notify(ScreenStateProperty::Height, (T const&)_height);
  }

  void
  size(T const& width, T const& height) {
    _width  = width;
    _height = height;
    notify(ScreenStateProperty::Width,  (T const&)_width);
    notify(ScreenStateProperty::Height, (T const&)_height);
  }

  T const&
  fovY() const {
    return _fovY;
  }

  void
  fovY(T const fovY) {
    _fovY = fovY;
    notify(ScreenStateProperty::FovY, (T const&)_fovY);
  }

  T const&
  nearZ() const {
    return _nearZ;
  }

  void
  nearZ(T const nearZ) {
    _nearZ = nearZ;
    notify(ScreenStateProperty::NearZ, (T const&)_nearZ);
  }

  T const&
  farZ() const {
    return _farZ;
  }

  void
  farZ(T const farZ) {
    _farZ = farZ;
    notify(ScreenStateProperty::FarZ, (T const&)_farZ);
  }

private:
  T _width;
  T _height;
  T _fovY;
  T _nearZ;
  T _farZ;
};
}
}

#endif
