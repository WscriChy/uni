#ifndef Uni_InputMapping_Integration_Qt_qtkeyTo_hpp
#define Uni_InputMapping_Integration_Qt_qtkeyTo_hpp

#include <Uni/InputMapping/Event>

namespace Uni {
namespace InputMapping {
namespace Integration {
namespace Qt {
InputMapping::Event const&
qtKeyToUpEvent(int const& code);

InputMapping::Event const&
qtKeyToDownEvent(int const& code);
}
}
}
}

#endif
