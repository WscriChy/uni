#ifndef Uni_InputMapping_MouseState
#define Uni_InputMapping_MouseState

#include <memory>

namespace Uni {
namespace InputMapping {
class MouseState {
public:
  typedef std::shared_ptr<MouseState> Shared;

public:
  MouseState() : _deltaX(0.0),
                 _deltaY(0.0),
                 _x(0.0),
                 _y(0.0),
                 _isLeftButtonPressed(false),
                 _isRightButtonPressed(false) {}

  MouseState(MouseState const& other) = delete;

  virtual
  ~MouseState() {}

  MouseState const&
  operator==(MouseState const& other) = delete;

  float const&
  deltaX() const {
    return _deltaX;
  }

  float const&
  deltaY() const {
    return _deltaY;
  }

  float const&
  x() const { return _x; }

  void
  x(float const& x) {
    _deltaX = x - _x;
    _x      = x;
  }

  float const&
  y() const { return _y; }

  void
  y(float const& y) {
    _deltaY = y - _y;
    _y      = y;
  }

  void
  position(float const& x, float const& y) {
    _deltaX = x - _x;
    _x      = x;
    _deltaY = y - _y;
    _y      = y;
  }

  bool
  isLeftButttonPressed() const { return _isLeftButtonPressed; }

  void
  isLeftButttonPressed(bool const& is) { _isLeftButtonPressed = is; }

  bool
  isRightButttonPressed() const { return _isRightButtonPressed; }

  void
  isRightButttonPressed(bool const& is) { _isRightButtonPressed = is; }

private:
  float _deltaX;
  float _deltaY;
  float _x;
  float _y;
  bool  _isLeftButtonPressed;
  bool  _isRightButtonPressed;
};
}
}
#endif
