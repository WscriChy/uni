#pragma once

#include <memory>

namespace Uni {
namespace Core {
class IterativeTask {
public:
  typedef std::shared_ptr<IterativeTask> Shared;

public:
  virtual
  ~IterativeTask() {}

  virtual
  void
  initialize() {}

  virtual
  void
  iterate() {}

  virtual
  void
  release() {}
};
}

using IterativeTask = Uni::Core::IterativeTask;
}
