#ifndef Uni_Core_Auxiliary_StdIteratorWrapper_hpp
#define Uni_Core_Auxiliary_StdIteratorWrapper_hpp

#include <Uni/IteratorFacade>

namespace Uni {
namespace Core {
namespace Auxiliary {
template <class StdIterator,
          class Type>
class StdIteratorWrapper : public IteratorFacade<Iterator<StdIterator, Type>,
                                                 Type> {
  friend class IteratorFacade<Iterator<StdIterator, Type>, Type>;

private:
  typedef IteratorFacade<Iterator<StdIterator, Type>, Type> Base;

public:
  typedef typename Base::Reference Reference;
  typedef typename Base::Pointer   Pointer;

public:
  StdIteratorWrapper() : _iterator() {}

  explicit
  StdIteratorWrapper(StdIterator const& iterator) : _iterator(iterator) {}

private:
  void
  increment() { ++_iterator; }

  void
  decrement() { --_iterator; }

  bool
  equals(Iterator const& other) const {
    return _iterator == other._iterator;
  }

  Reference
  dereference() const {
    return *_iterator;
  }

private:
  StdIterator _iterator;
};

#endif
