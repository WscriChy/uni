#pragma once

#include "IterativeTask.hpp"
#include "Observable.hpp"

#include <Uni/Logging/macros>

#include <atomic>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <thread>

namespace Uni {
namespace Core {
enum class IterativeWorkerProperties {
  InitializationFinished = 0,
  IterationFinished      = 1,
  ReleaseFinished        = 2,
};
class IterativeWorker : public Observable<IterativeWorkerProperties> {
public:
  enum class State {
    Na                              = -1,
    Initializing                    = 1,
    Iterating                       = 2,
    Releasing                       = 3,
    WaitingAtInitializationFinished = 4,
    WaitingAtIterationFinished      = 5,
    WaitingAtReleaseFinished        = 6,
    Paused                          = 7,
    Stopped                         = 8,
  };

private:
  enum class Command {
    Na    = -1,
    Stop  = 0,
    Pause = 1,
  };

public:
  typedef std::shared_ptr<IterativeWorker> Shared;

private:
  typedef std::unique_lock<std::mutex> UniqueLock;

public:
  IterativeWorker() : _state(State::Stopped),
    _command(Command::Na),
    _skipWaitAtInitializationFinished(false),
    _isWaitAtInitializationFinished(false),
    _skipWaitAtIterationFinished(false),
    _isWaitAtIterationFinished(false),
    _skipWaitAtReleaseFinished(false),
    _isWaitAtReleaseFinished(false) {}

  IterativeWorker(IterativeWorker const& other) = delete;

  virtual
  ~IterativeWorker() { stop(); }

  IterativeWorker const&
  operator=(IterativeWorker const& other) = delete;

  bool
  isWaitAtInitializationFinished() {
    UniqueLock atomicLock(_atomicMutex);

    return _isWaitAtInitializationFinished.load();
  }

  void
  isWaitAtInitializationFinished(bool const& is) {
    UniqueLock atomicLock(_atomicMutex);

    if (!is) {
      unguardedEndWaitAtInitializationFinished();
    }
    _isWaitAtInitializationFinished.store(is);
  }

  void
  endWaitAtInitializationFinished() {
    UniqueLock atomicLock(_atomicMutex);
    unguardedEndWaitAtInitializationFinished();
  }

  bool
  isWaitAtIterationFinished() {
    UniqueLock atomicLock(_atomicMutex);

    return _isWaitAtIterationFinished.load();
  }

  void
  isWaitAtIterationFinished(bool const& is) {
    UniqueLock atomicLock(_atomicMutex);

    if (!is) {
      unguardedEndWaitAtIterationFinished();
    }
    _isWaitAtIterationFinished.store(is);
  }

  void
  endWaitAtIterationFinished() {
    UniqueLock atomicLock(_atomicMutex);
    unguardedEndWaitAtIterationFinished();
  }

  bool
  isWaitAtReleaseFinished() {
    UniqueLock atomicLock(_atomicMutex);

    return _isWaitAtReleaseFinished.load();
  }

  void
  isWaitAtReleaseFinished(bool const& is) {
    UniqueLock atomicLock(_atomicMutex);

    if (!is) {
      unguardedEndWaitAtReleaseFinished();
    }
    _isWaitAtReleaseFinished.store(is);
  }

  void
  endWaitAtReleaseFinished() {
    UniqueLock atomicLock(_atomicMutex);
    unguardedEndWaitAtReleaseFinished();
  }

  IterativeTask::Shared
  task() const {
    return _task;
  }

  void
  task(IterativeTask::Shared task) {
    stop();
    _task = task;
  }

  State
  state() const {
    return _state.load();
  }

  void
  run() {
    _task->initialize();
    notify(IterativeWorkerProperties::InitializationFinished);
    startWaitAtInitializationFinished();

    while (_command != Command::Stop) {
      UniqueLock atomicLock(_atomicMutex);

      if (_command == Command::Pause) {
        _command = Command::Na;
        _state   = State::Paused;
        _conditionPause.wait(atomicLock, [this] () {
                               return _state !=
                               State::Paused;
                             });
      }
      _state = State::Iterating;
      atomicLock.unlock();
      _task->iterate();
      notify(IterativeWorkerProperties::IterationFinished);
      startWaitAtIterationFinished();
    }
    _state = State::Releasing;
    _task->release();
    notify(IterativeWorkerProperties::ReleaseFinished);
    startWaitAtReleaseFinished();
    _conditionStop.notify_all();
  }

  void
  start() {
    UniqueLock atomicLock(_atomicMutex);

    if (_state != State::Stopped) {
      return;
    }
    _state  = State::Initializing;
    _thread = std::thread(&IterativeWorker::run, this);
  }

  void
  stop() {
    UniqueLock atomicLock(_atomicMutex);

    if (_state == State::Stopped) {
      return;
    }
    _command = Command::Stop;

    unguardedUnpause();
    unguardedEndWaitAtInitializationFinished();
    unguardedEndWaitAtIterationFinished();
    unguardedEndWaitAtReleaseFinished();

    _skipWaitAtInitializationFinished = false;
    _skipWaitAtIterationFinished      = false;
    _skipWaitAtReleaseFinished        = false;

    _conditionStop.wait(atomicLock);

    _thread.join();

    _state = State::Stopped;
  }

  void
  pause() {
    UniqueLock atomicLock(_atomicMutex);

    if (_command == Command::Na && _state != State::Stopped) {
      _command = Command::Pause;
    }
  }

  void
  unpause() {
    UniqueLock atomicLock(_atomicMutex);
    unguardedUnpause();
  }

private:
  void
  startWaitAtInitializationFinished() {
    UniqueLock atomicLock(_atomicMutex);

    if (_skipWaitAtInitializationFinished) {
      _skipWaitAtInitializationFinished = false;

      return;
    }

    if (_command == Command::Stop) {
      return;
    }

    if (_isWaitAtInitializationFinished == true) {
      _state = State::WaitingAtInitializationFinished;
      _waitAtInitializationFinished.wait(atomicLock,
                                         [this] () {
                                           return _state !=
                                           State::
                                           WaitingAtInitializationFinished;
                                         });
    }
  }

  void
  startWaitAtIterationFinished() {
    UniqueLock atomicLock(_atomicMutex);

    if (_skipWaitAtIterationFinished) {
      _skipWaitAtIterationFinished = false;

      return;
    }

    if (_command == Command::Stop) {
      return;
    }

    if (_isWaitAtIterationFinished == true) {
      _state = State::WaitingAtIterationFinished;
      _waitAtIterationFinished.wait(atomicLock,
                                    [this] () {
                                      return _state !=
                                      State::
                                      WaitingAtIterationFinished;
                                    });
    }
  }

  void
  startWaitAtReleaseFinished() {
    UniqueLock atomicLock(_atomicMutex);

    if (_skipWaitAtReleaseFinished) {
      _skipWaitAtReleaseFinished = false;

      return;
    }

    if (_command == Command::Stop) {
      return;
    }

    if (_isWaitAtReleaseFinished == true) {
      _state = State::WaitingAtReleaseFinished;
      _waitAtReleaseFinished.wait(atomicLock,
                                  [this] () {
                                    return _state !=
                                    State::
                                    WaitingAtReleaseFinished;
                                  });
    }
  }

  void
  unguardedEndWaitAtInitializationFinished() {
    if (_state == State::WaitingAtInitializationFinished) {
      _state = State::Na;
      _waitAtInitializationFinished.notify_all();
    } else if (_state == State::Initializing) {
      _skipWaitAtInitializationFinished = true;
    }
  }

  void
  unguardedEndWaitAtIterationFinished() {
    if (_state == State::WaitingAtIterationFinished) {
      _state = State::Na;
      _waitAtIterationFinished.notify_all();
    } else if (_state == State::Iterating || _state == State::Paused) {
      _skipWaitAtIterationFinished = true;
    }
  }

  void
  unguardedEndWaitAtReleaseFinished() {
    if (_state == State::WaitingAtReleaseFinished) {
      _state = State::Na;
      _waitAtReleaseFinished.notify_all();
    } else if (_state != State::Releasing) {
      _skipWaitAtReleaseFinished = true;
    }
  }

  void
  unguardedUnpause() {
    if (_state == State::Paused) {
      _state = State::Na;
      _conditionPause.notify_all();
    } else if (_command == Command::Pause) {
      _command = Command::Na;
    }
  }

private:
  IterativeTask::Shared _task;
  std::atomic<State>    _state;
  std::atomic<Command>  _command;

  std::thread _thread;
  std::mutex  _atomicMutex;

  bool              _skipWaitAtInitializationFinished;
  std::atomic<bool> _isWaitAtInitializationFinished;
  bool              _skipWaitAtIterationFinished;
  std::atomic<bool> _isWaitAtIterationFinished;
  bool              _skipWaitAtReleaseFinished;
  std::atomic<bool> _isWaitAtReleaseFinished;

  std::condition_variable _waitAtInitializationFinished;
  std::condition_variable _waitAtIterationFinished;
  std::condition_variable _waitAtReleaseFinished;

  std::condition_variable _conditionPause;

  std::condition_variable _conditionStop;
};
}

using IterativeWorker           = Uni::Core::IterativeWorker;
using IterativeWorkerProperties = Uni::Core::IterativeWorkerProperties;
}
