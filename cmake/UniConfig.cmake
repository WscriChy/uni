if (CMAKE_VERSION VERSION_LESS 2.8.3)
  message(FATAL_ERROR "Uni requires at least CMake version 2.8.3")
endif()

if (WIN32)
  get_filename_component(_Uni_INSTALL_PREFIX "${CMAKE_CURRENT_LIST_DIR}/../" ABSOLUTE)
else()
  get_filename_component(_Uni_INSTALL_PREFIX "${CMAKE_CURRENT_LIST_DIR}/../../../" ABSOLUTE)
endif ()

set(Uni_VERSION_STRING 0.1.0)

macro(_uni_check_file_exists file Target)
  if(NOT EXISTS "${file}" )
    message(FATAL_ERROR "The imported target \"${Target}\" references the file
    \"${file}\"
    but this file does not exist.  Possible reasons include:
    * The file was deleted, renamed, or moved to another location.
    * An install or uninstall procedure did not complete successfully.
    * The installation package was faulty and contained
    \"${CMAKE_CURRENT_LIST_FILE}\"
    but not all the files it references.")
  endif()
endmacro()

macro(_populate_uni_target_properties Target Configuration Dependencies LIB_LOCATION)
  set_property(TARGET ${Target} APPEND PROPERTY IMPORTED_CONFIGURATIONS ${Configuration})
  set_target_properties(${Target} PROPERTIES
    "IMPORTED_LINK_INTERFACE_LIBRARIES_${Configuration}" "${Dependencies}"
    "IMPORTED_LOCATION_${Configuration}" ${LIB_LOCATION}
    )
  if(NOT "${IMPLIB_LOCATION}" STREQUAL "")
      set_target_properties(${Target} PROPERTIES
      "IMPORTED_IMPLIB_${Configuration}" ${IMPLIB_LOCATION}
      )
  endif()
endmacro()

if (NOT TARGET Uni)
  set(Uni_FOUND False)

  add_library(Pugi STATIC IMPORTED)
  add_library(Uni STATIC IMPORTED)
  add_library(GTest STATIC IMPORTED)
  add_library(GTestMain STATIC IMPORTED)

  set(Pugi_LIBRARY_DIR "${_Uni_INSTALL_PREFIX}/lib")
  find_library(Pugi_LIBRARY
             NAMES Pugi
             PATHS ${Pugi_LIBRARY_DIR})
  _populate_uni_target_properties(Pugi RELEASE "" ${Pugi_LIBRARY})

  set(Uni_LIBRARY_DIR "${_Uni_INSTALL_PREFIX}/lib")
  find_library(Uni_LIBRARY
             NAMES Uni
             PATHS ${Uni_LIBRARY_DIR})
  _populate_uni_target_properties(Uni RELEASE "" ${Uni_LIBRARY})

  find_package(Threads REQUIRED)
  set(GTest_LIBRARY_DIR "${_Uni_INSTALL_PREFIX}/lib")
  set(GTest_Main_LIBRARY_DIR "${_Uni_INSTALL_PREFIX}/lib")
  find_library(GTest_LIBRARY
             NAMES gtest
             PATHS ${GTest_LIBRARY_DIR})
  _populate_uni_target_properties(GTest RELEASE "${CMAKE_THREAD_LIBS_INIT}" ${GTest_LIBRARY})

  set(GTest_Main_LIBRARY_DIR "${_Uni_INSTALL_PREFIX}/lib")
  find_library(GTest_Main_LIBRARY
             NAMES gtest_main
             PATHS ${GTest_Main_LIBRARY_DIR})
  _populate_uni_target_properties(GTestMain RELEASE "" ${GTest_Main_LIBRARY})


  _populate_uni_target_properties(Pugi DEBUG "" ${Pugi_LIBRARY})
  _populate_uni_target_properties(Uni DEBUG "" ${Uni_LIBRARY})
  _populate_uni_target_properties(GTest DEBUG "" ${GTest_LIBRARY})
  _populate_uni_target_properties(GTestMain DEBUG "" ${GTest_Main_LIBRARY})


  list(APPEND Pugi_INCLUDE_DIRS "${_Uni_INSTALL_PREFIX}/include")
  list(APPEND Uni_INCLUDE_DIRS "${_Uni_INSTALL_PREFIX}/include")
  list(APPEND GTest_INCLUDE_DIRS "${_Uni_INSTALL_PREFIX}/include")
  list(APPEND GTest_Main_INCLUDE_DIRS "${_Uni_INSTALL_PREFIX}/include")

  _uni_check_file_exists(${Pugi_INCLUDE_DIRS} Pugi)
  _uni_check_file_exists(${Uni_INCLUDE_DIRS} Uni)
  _uni_check_file_exists(${GTest_INCLUDE_DIRS} GTest)
  _uni_check_file_exists(${GTest_Main_INCLUDE_DIRS} GTestMain)

  set_property(TARGET Pugi PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${Pugi_INCLUDE_DIRS})
  set_property(TARGET Uni PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${Uni_INCLUDE_DIRS})
  set_property(TARGET GTest PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${GTest_INCLUDE_DIRS})
  set_property(TARGET GTestMain PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${GTest_Main_INCLUDE_DIRS})

  set_property(TARGET Uni PROPERTY INTERFACE_COMPILE_DEFINITIONS ${Uni_DEFENITIONS})

  set(Uni_FOUND True)

  message(STATUS "Found Uni Library: ${Pugi_LIBRARY} ${Uni_LIBRARY} ${GTest_LIBRARY} ${GTest_Main_LIBRARY}")
endif()
